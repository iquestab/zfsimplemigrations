<?php
namespace ZfSimpleMigrations\Controller;

use Zend\Mvc\Console\Controller\AbstractConsoleController;

use Zend\Mvc\MvcEvent;
use Zend\Console\Request as ConsoleRequest;
use ZfSimpleMigrations\Library\Migration;
use ZfSimpleMigrations\Library\MigrationException;
use ZfSimpleMigrations\Library\MigrationSkeletonGenerator;
use ZfSimpleMigrations\Library\OutputWriter;

/**
 * Migration commands controller
 */
class MigrateController extends AbstractConsoleController
{
    /** @var  Migration[] $migrations */
    protected $migrations;

    /** @var  MigrationSkeletonGenerator[] $skeletonGenerators */
    protected $skeletonGenerators;

    /** @var  OutputWriter $outputWriter */
    protected $outputWriter;

    public function onDispatch(MvcEvent $e)
    {
        if (!$this->getRequest() instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }

        return parent::onDispatch($e);
    }

    /**
     * @return \Zend\Stdlib\RequestInterface
     */
    public function getRequest()
    {
        return parent::getRequest();
    }

    /**
     * Get current migration version
     *
     * @return int
     */
    public function versionAction()
    {
        $list = [];
        foreach ($this->getMigrations() as $module => $migration) {
            $list[] = sprintf("Current version of %s is %s\n", $module, $migration->getCurrentVersion());
        }
        return implode("\n", $list);
    }

    /**
     * List migrations - not applied by default, all with 'all' flag.
     *
     * @return string
     */
    public function listAction()
    {
        $migrations = $this->getVersions($this->getRequest()->getParam('all'));
        $list = [];
        foreach ($migrations as $m) {
            $list[] = sprintf("%s %s@%s - %s", $m['applied'] ? '-' : '+', $m['version'], $m['module'], $m['description']);
        }
        return (empty($list) ? 'No migrations available.' : implode("\n", $list)) . "\n";
    }

    /**
     * Apply migration
     */
    public function applyAction()
    {
        $version = $this->getRequest()->getParam('version');
        $force = $this->getRequest()->getParam('force');
        $down = $this->getRequest()->getParam('down');
        $fake = $this->getRequest()->getParam('fake');
        $name = $this->getRequest()->getParam('name');

        if (is_null($version) && $force) {
            return "Can't force migration apply without migration version explicitly set.";
        }
        if (is_null($version) && $fake) {
            return "Can't fake migration apply without migration version explicitly set.";
        }

        if ($down) {
            $force = true;
        }
        $executeType = $down ? 'Rollback' : 'Apply';
        $migrations = $this->getVersions($force || $down);
        if ($down) {
            $migrations = array_reverse($migrations);
        }

        $applied = [];
        foreach ($migrations as $migration) {
            $ver = $migration['version'];
            $module = $migration['module'];

            if ($down && (!$migration['applied'] || (!is_null($version) && $ver <= $version))) {
                // Ignore unmigrated version as well as the older than target version
                continue;
            }

            // Case want to run specific version
            if (!$down && !is_null($version) && $ver != $version) {
                continue;
            }

            try {
                $this->outputWriter->writeLine(sprintf('Executing %s version %s of module %s', $executeType, $ver, $module));
                $this->getMigration($module)->migrate($migration['version'], $force, $down, $fake);
                $this->outputWriter->writeLine(sprintf('Done for version %s of module %s', $ver, $module));
                array_unshift($applied, $migration);
            } catch (MigrationException $e) {
                $this->outputWriter->writeLine(sprintf('%s failed for version %s of module %s, will undo executed ones', $executeType, $ver, $module));
                foreach ($applied as $m) {
                    $this->outputWriter->writeLine(sprintf('Undo version %s of module %s', $m['version'], $m['module']));
                    $this->getMigration($module)->migrate($migration['version'], true, !$down, $fake);
                }
                throw $e;
            }
        }

        return "Migrations done!\n";
    }

    /**
     * Generate new migration skeleton class
     */
    public function generateSkeletonAction()
    {
        $list = [];
        foreach ($this->getSkeletonGenerators() as $module => $generator) {
            $classPath = $generator->generate();
            $list[] = sprintf("Generated skeleton class @ %s\n", realpath($classPath));
        }

        return implode("\n", $list);
    }

    /**
     * @param $name
     * @return Migration $migration
     */
    public function getMigration($name) {
        $migration = $this->getMigrations()[$name];
        return $migration;
    }

    /**
     * @return Migration[]
     */
    public function getMigrations()
    {
        return $this->migrations;
    }

    /**
     * @param Migration[] $migrations
     */
    public function setMigrations($migrations)
    {
        $this->migrations = $migrations;
    }

    /**
     * @return MigrationSkeletonGenerator[]
     */
    public function getSkeletonGenerators()
    {
        return $this->skeletonGenerators;
    }

    /**
     * @param MigrationSkeletonGenerator[] $skeletonGenerators
     */
    public function setSkeletonGenerators($skeletonGenerators)
    {
        $this->skeletonGenerators = $skeletonGenerators;
    }

    /**
     * @param OutputWriter $output
     */
    public function setOutputWriter($output)
    {
        $this->outputWriter = $output;
    }

    protected function getVersions($all = false) {
        $versions = [];
        foreach ($this->getMigrations() as $name => $migration) {
            $vers = $migration->getMigrationClasses($all);
            foreach ($vers as $v) {
                $v['module'] = $name;
                array_push($versions, $v);
            }
        }

        uasort($versions, function ($a, $b) {
            if ($a['version'] == $b['version']) {
                return 0;
            }

            return ($a['version'] < $b['version']) ? -1 : 1;
        });

        return $versions;
    }
}
