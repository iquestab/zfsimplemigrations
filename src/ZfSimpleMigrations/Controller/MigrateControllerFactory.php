<?php


namespace ZfSimpleMigrations\Controller;


use Interop\Container\ContainerInterface;
use Zend\Mvc\Console\Router\RouteMatch;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfSimpleMigrations\Library\Migration;
use ZfSimpleMigrations\Library\MigrationSkeletonGenerator;
use ZfSimpleMigrations\Library\OutputWriter;

class MigrateControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        /** @var Migration[] $migrations */
        $migrations = [];
        /** @var MigrationSkeletonGenerator[] $generators */
        $generators = [];

        /** @var RouteMatch $routeMatch */
        $routeMatch = $container->get('Application')->getMvcEvent()->getRouteMatch();

        $name            = $routeMatch->getParam('name', 'all');
        if ($name == 'all') {
            $name = '*';
        }

        $name = str_replace('*', '(.*)', $name);
        $name = '/^'.$name.'$/';

        $migrationConfigs = $container->get('config')['migrations'];
        foreach ($migrationConfigs as $module => $migrationConfig) {
            if (!preg_match($name, $module, $matches)) {
                print_r("not matched\n\n");
                continue;
            }

            $prefix          = isset($migrationConfig['prefix']) ? $migrationConfig['prefix'] : '';

            /** @var Migration $migration */
            $migration = $container->get('migrations.migration.' . $module);
            $migration->changeMigrationPrefix($prefix);

            /** @var MigrationSkeletonGenerator $generator */
            $generator = $container->get('migrations.skeleton-generator.' . $module);

            $migrations[$module] = $migration;
            $generators[$module] = $generator;
        }

        //$migrationConfig = $container->get('config')['migrations'][$name];
//        $prefix          = isset($migrationConfig['prefix']) ? $migrationConfig['prefix'] : '';
//
//        /** @var Migration $migration */
//        $migration = $container->get('migrations.migration.' . $name);
//        $migration->changeMigrationPrefix($prefix);
//
//        /** @var MigrationSkeletonGenerator $generator */
//        $generator = $container->get('migrations.skeleton-generator.' . $name);

        $console = $container->get('console');
        $output = new OutputWriter(function ($message) use ($console) {
            $console->write($message . "\n");
        });

        $controller = new MigrateController();

        $controller->setOutputWriter($output);
        $controller->setMigrations($migrations);
        $controller->setSkeletonGenerators($generators);

        return $controller;
    }


}
